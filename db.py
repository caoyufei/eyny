import queue

import pymysql
from pymysql.cursors import DictCursor


class DB:

    def __init__(self, maxsize=3):
        self.conn = None
        self._queue = queue.Queue(maxsize=maxsize)
        for i in range(maxsize):
            self._queue.put(self._create())

    def _create(self):
        return pymysql.connect("localhost", "python", "hack3321", "avhub", charset='utf8')

    def _put(self, conn):
        self._queue.put(conn)

    def _get(self):
        conn = self._queue.get()
        if conn is None:
            return self._create()
        return conn

    def __del__(self):
        while not self._queue.empty():
            conn = self._queue.get_nowait()
            if conn:
                conn.close()

    def get_mgstage_count(self):
        sql = 'select count(*) as c from ii_mgstage'
        data = self.execute(sql)
        if data:
            return data['c']
        return 0

    def get_mgstage(self, title=None):
        sql = "select * from ii_mgstage"
        params = ()
        return_one = False
        if title:
            sql = '%s %s' % (sql, 'where title = %s limit 1')
            params = (title,)
            return_one = True
        return self.execute(sql, params, return_one)

    def get_eyny(self, title):
        sql = "select * from ii_eyny where title = %s limit 1"
        params = (title,)
        return self.execute(sql, params)

    def get_download(self, title=None, limit=10):
        sql = "select * from ii_eyny where files = '' and openload_url = '' and down_list <> '' ORDER BY rand() limit %s"
        params = (limit,)
        return_one = False
        if title:
            sql = "select * from ii_eyny where title = %s"
            params = (title,)
            return_one = True
        return self.execute(sql, params, return_one)

    def execute(self, sql, params=(), return_one=True):
        conn = self._get()
        try:
            with conn.cursor(cursor=DictCursor) as cursor:
                # cursor = conn.cursor(cursor=DictCursor)
                cursor.execute(sql, params)
                conn.commit()
                if sql.lower().find('insert into') != -1:
                    return cursor.lastrowid
                if sql.lower().find('update') != -1:
                    return cursor.rowcount
                return cursor.fetchone() if return_one else cursor.fetchall()
        except Exception as e:
            if conn:
                conn.rollback()
            print('[%s] error: %s' % (sql, e))
        finally:
            if conn:
                self._put(conn)
            else:
                print('conn Already closed')
        return None

    def get_google_drive_list(self):
        sql = "select * from ii_eyny where is_google = 1"
        cursor = self.conn.cursor()
        rows = cursor.execute(sql)
        self.conn.commit()
        return rows
