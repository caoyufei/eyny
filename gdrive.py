# -*- coding: utf8 -*-
import os
import socket

import socks
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive

from common import PROXIES, ROOT_PATH_VIDEO, file_is_video
from db import DB

os.environ['TZ'] = 'Asia/Shanghai'


class GDrive:

    def __init__(self):
        self.db = DB()

    def _drive_auth(self):
        if PROXIES:
            socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", 1080)
            socket.socket = socks.socksocket

        auth = GoogleAuth()
        auth.CommandLineAuth()
        self.drive = GoogleDrive(auth)

    def count(self):
        self._drive_auth()
        file_list = self.drive.ListFile().GetList()
        count = 0
        for file in file_list:
            title = file['title']
            if title.find('mp4') != -1:
                count = count + 1
        print('google file count: %s' % count)

    def reset(self):
        sql = 'select * from ii_eyny where is_google = 1'
        items = self.db.execute(sql, (), False)

        self._drive_auth()
        file_list = self.drive.ListFile().GetList()
        for item in items:
            title = item['title']
            flag = False
            for file in file_list:
                _title = file['title']
                if _title.find(title) != -1:
                    flag = True
                    break
            if not flag:
                sql = 'update ii_eyey set is_google = 0 where id = %s'
                self.db.execute(sql, (item['id'],))

    def get_upload_file(self):
        items = []
        rs = os.popen("find %s -type f" % ROOT_PATH_VIDEO)
        files = rs.readlines()
        for file in files:
            file = file.replace("\n", "")
            if not os.path.isfile(file) or not file_is_video(file):
                continue

            file_info = file.split('/')
            title = file_info[-2]
            data = self.db.get_eyny(title)
            if not data or data['is_google'] > 0:
                continue

            items.append({'file': file, 'title': title})
        return items

    def start_upload(self):
        file_list = self.get_upload_file()
        if len(file_list) > 20:
            file_list = file_list[0:20]

        for item in file_list:
            local_file = item['file']
            title = item['title']

            print('start upload file [%s] to google drive' % local_file)
            cmd = 'rclone copy %s pod78:%s' % (local_file, '/mgstage/')
            os.system(cmd)

            sql = 'update ii_eyny set is_google = 1 where title = %s'
            row = self.db.execute(sql, (title,))
            print('[%s] upload to google drive %s' % (title, row))
