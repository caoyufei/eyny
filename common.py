import codecs
import hashlib
import json
import os
import random
import re
import shlex
import socket
import subprocess
import time

import pyquery
import requests
from requests.adapters import HTTPAdapter

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("8.8.8.8", 80))
ip = s.getsockname()[0]
s.close()

if ip.find('10.3.19') != -1 or ip.find('192.168') != -1 or ip.find('10.0.2') != -1:
    PROXIES = {
        'http': 'socks5://127.0.0.1:1080',
        'https': 'socks5://127.0.0.1:1080',
    }
else:
    PROXIES = None

BASE_API = 'http://jav-hub.net' if PROXIES else 'http://www.avhub8.com'

BASE_URL = 'http://www23.eyny.com/'

s = requests.session()
a = HTTPAdapter(max_retries=0, pool_maxsize=20)
s.mount('http://', a)
s.mount('https://', a)

HEADERS = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36',
    'Referer': BASE_URL
}

LOG_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'logs')
if not os.path.isdir(LOG_PATH):
    os.makedirs(LOG_PATH)

MGSTAGE_PATH = LOG_PATH if PROXIES else '/data/wwwroot/avhub/uploads'

DIR_NAME = 'mgstage'
DIR_VIDEO_NAME = 'mgstage_2'

ROOT_PATH = os.path.join('/data', DIR_NAME)
if not os.path.isdir(ROOT_PATH):
    os.makedirs(ROOT_PATH)

ROOT_PATH_VIDEO = os.path.join('/data', DIR_VIDEO_NAME)
if not PROXIES and not os.path.isdir(ROOT_PATH_VIDEO):
    os.makedirs(ROOT_PATH_VIDEO)


def fetch(url, data=None, **kwargs):
    response = None
    kwargs.setdefault('headers', HEADERS)
    kwargs.setdefault('timeout', 20)
    kwargs.setdefault('proxies', PROXIES)

    for i in range(1, 4):
        try:
            if data is None:
                response = s.get(url, **kwargs)
            else:
                response = s.post(url, data, **kwargs)
            if response.ok:
                return response
        except Exception as e:
            time.sleep(1)
            print('retry [%s] %s %s' % (i, url, e))
            continue
    return response


def google_down_file(url, path):
    down_id = re.search('id=(.+)', url).group(1)

    base_url = 'https://drive.google.com'
    down_url = base_url + '/uc?id=' + down_id + '&export=download'
    res = fetch(down_url, allow_redirects=False)
    if not res:
        return None

    if res.status_code == 302:
        res2 = fetch(url)
        if not res2:
            return None
        doc = pyquery.PyQuery(res2.text)
        name = doc('meta[itemprop="name"]').attr('content')
        if not name:
            return None
        down_link = res.headers.get('Location')
    else:
        doc = pyquery.PyQuery(res.text)
        link = doc('#uc-download-link').attr('href')
        name = doc('.uc-warning-subcaption a').text()
        if not name:
            return None

        res2 = fetch(base_url + link, allow_redirects=False)
        down_link = res2.headers.get('Location')

    file = os.path.join(path, name)
    if os.path.exists(file) and os.path.getsize(file) > 10:
        return file

    if down_link:
        print('     download [%s]: %s' % (name, down_link))
        # _down_file(down_link, file)
        down_cmd = "wget -c -nv -O %s '%s'" % (file, down_link)
        os.system(down_cmd)
        return file

    # print(res.text)
    return None


def _down_file(url, file):
    res = fetch(url)
    with open(file, 'wb') as f:
        f.write(res.content)


def wget_down_file(url, file_name):
    if PROXIES:
        return True
    if os.path.isfile(file_name) and os.path.getsize(file_name) > 100:
        return True
    cmd = 'wget -q -O %s %s' % (file_name, url)
    os.system(cmd)
    if os.path.isfile(file_name) and os.path.getsize(file_name) > 100:
        return True

    print('downloading %s fail!' % url)
    write_file('%s\n' % cmd, os.path.join(LOG_PATH, 'wget.log'), 'a')
    os.remove(file_name)
    return False


def get_file_size(file):
    plain_size = float(os.path.getsize(file))
    if plain_size <= 1024:
        return str(round(plain_size, 2)) + 'B'
    if plain_size <= 1024 * 1024:
        return str(round(plain_size / 1024, 2)) + 'K'
    if plain_size <= 1024 * 1024 * 1024:
        return str(round(plain_size / 1024 / 1024, 2)) + 'M'
    if plain_size <= 1024 * 1024 * 1024 * 1024:
        return str(round(plain_size / 1024 / 1024 / 1024, 2)) + 'G'


def fm_vip_down(url, path):
    cookies = '__cfduid=d8654e2a976dd42b48796e19993898ab61524539405; PHPSESSID=nhlirshk9jp63m3e2lm5iioec1; userlurl=http://www.feemoo.com/home.php#/svip; phpdisk_zcore_v2_info=23e8b0DiUQTArZnI874AJe7CvqgMQwDB9kmlohE7tUcHhEaXxedf%2Bo%2FVAS6ZNyTtoFxJopVoAjNHecFfoIVevI1Ra2Yo9%2BvaSA0G3AtmZCnvuMMIjyiTKOMXHrXtxL0NWZj2QjFB; view_stat=1'
    _headers = HEADERS.copy()
    _headers['Cookie'] = cookies
    _headers['X-Requested-With'] = 'XMLHttpRequest'
    _headers['Referer'] = url

    res = fetch(url, headers=_headers)
    if not res:
        return None

    doc = pyquery.PyQuery(res.text)
    title = doc('.down_one_lf_tl').text()

    file = os.path.join(path, title)
    if os.path.isfile(file) and os.path.getsize(file) > 10:
        return file

    file_id = re.search('(\d+)', url).group(1)
    params = {'action': 'load_down_addr_svip', 'file_id': file_id}
    res = fetch('http://www.feemoo.com/yythems_ajax.php', data=params, headers=_headers)
    data = res.text
    if data.find('"status":true') == -1:
        print(res.text)
        return None

    data = json.loads(data)
    for i in range(3):
        wget_proxy = ''
        # if i > 0:
        #     wget_proxy = '-e http_proxy=http://205.139.16.113:8082'
        down_cmd = "wget %s -c -nv -O %s '%s'" % (wget_proxy, file, data['str'])
        os.system(down_cmd)
        if os.path.getsize(file) > 100:
            return file
        os.remove(file)
        time.sleep(1)

    # _down_file(data['str'], file)
    return None


def print_files():
    file_list = list()
    video_list = dict()
    rs = os.popen("find %s -type f" % ROOT_PATH)
    files = rs.readlines()
    for file in files:
        file = file.replace("\n", "")
        if not os.path.isfile(file):
            print(file)
            continue

        if file_is_video(file):
            video_list[file] = os.path.getsize(file)
        else:
            if file.find('rar') == -1:
                os.remove(file)
                continue
            file_list.append({'file': file, 'size': get_file_size(file)})

    video_list = sorted(video_list.items(), key=lambda d: d[1], reverse=True)

    for item in video_list:
        # video_info = get_video_info(item[0])
        print("%s:   %s" % (get_file_size(item[0]), item[0]))
        # print("%s:   %s,  %s" % (get_file_size(item[0]), item[0], video_info))

    print('count: %s' % len(video_list))
    print('--------------------------------------')

    rs = os.popen("find %s -type f" % ROOT_PATH_VIDEO)
    files = rs.readlines()
    count = 0
    for file in files:
        file = file.replace("\n", "")
        if not os.path.isfile(file):
            continue
        count = count + 1
        print("%s:   %s" % (get_file_size(file), file))
    print('count: %s' % count)
    print('--------------------------------------')

    for item in file_list:
        print("%s:   %s" % (item['size'], item['file']))


def file_is_video(file):
    cmd = shlex.split('file --mime-type {0}'.format(file))
    result = str(subprocess.check_output(cmd))
    mimi_type = result.split()[-1]
    return mimi_type.find('video') != -1


def get_video_info(file):
    p = subprocess.Popen('ffmpeg -i %s' % file, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    (stdout, stderr) = p.communicate()
    stdout = str(stdout)

    find = re.search('Duration(.*?)Metadata', stdout)
    if not find:
        return None

    info = find.group(1)
    v_code = re.search('Video: (.+?) \(', info)
    v_duration = re.search('(\d{2}:\d{2}:\d{2}.\d{2})', info)
    v_bit_rate = re.search('bitrate: (\d+) kb', info)
    v_width = re.search('((\d{2,4})x(\d{2,4}))', stdout)
    data = list()
    if v_code:
        data.append(v_code.group(1))

    if v_bit_rate:
        data.append(v_bit_rate.group(1))
    if v_duration:
        data.append(v_duration.group(1))
    if v_width:
        data.append(v_width.group(1))
    return data


def md5(string):
    _s = hashlib.md5(string.encode('utf-8')).hexdigest()
    return str(_s[8:-8]).upper()


def random_str(n=5):
    data = random.sample('abcdefghijklmnopqrstuvwxyz123456789', n)
    return ''.join(data)


def reset_file(path):
    for (root, dirs, files) in os.walk(path):
        for name in files:
            file = os.path.join(root, name)
            if not file_is_video(file):
                continue
            info = file.split('/')
            title = info[-2]
            name_info = name.split('.')

            new_name = '%s.%s' % (title, name_info[1])
            new_file = os.path.join(root, new_name)
            if not os.path.isfile(new_file):
                print(file)
                os.system("mv %s %s" % (file, new_file))


def write_file(data, file, mode='w'):
    with codecs.open(file, encoding='utf-8', mode=mode) as f:
        f.write(data)
