import os
from concurrent import futures

from common import ROOT_PATH_VIDEO, file_is_video
from db import DB


class OndDrive:

    def __init__(self):
        self.db = DB()

    def start_update(self):
        file_list = self.get_upload_file()
        if len(file_list) > 50:
            file_list = file_list[0:50]

        with futures.ThreadPoolExecutor(5) as executor:
            executor.map(self._do_upload, file_list)

    def _do_upload(self, item):
        local_file = item['file']
        title = item['title']

        print('start upload file [%s] to one drive' % local_file)
        cmd = '/usr/bin/php /data/wwwroot/default/oneindex/one.php upload:file %s /mgstage/' % local_file
        os.system(cmd)

        sql = 'update ii_eyny set is_onedrive = 1 where title = %s'
        row = self.db.execute(sql, (title,))
        print('[%s] upload to one drive %s' % (title, row))

    def get_upload_file(self):
        items = []
        rs = os.popen("find %s -type f" % ROOT_PATH_VIDEO)
        files = rs.readlines()
        for file in files:
            file = file.replace("\n", "")
            if not os.path.isfile(file) or not file_is_video(file):
                continue

            file_info = file.split('/')
            title = file_info[-2]
            data = self.db.get_eyny(title)
            if not data or data['is_onedrive'] > 0:
                continue

            items.append({'file': file, 'title': title})
        return items
