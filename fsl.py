import json
import os
import random
import re
import time

import requests
from PIL import Image

from rk import RClient

fsl_list = ['http://fsl.to/6kdka2', 'http://fsl.to/dlgjw9', 'http://fsl.to/le1al6', 'http://fsl.to/c6fjn6',
            'http://fsl.to/f3m967', 'http://fsl.to/2l6oph', 'http://fsl.to/htl959', 'http://fsl.to/6l35ys',
            'http://fsl.to/ngnmrz', 'http://fsl.to/n8onnt', 'http://fsl.to/asiirc', 'http://fsl.to/czgwyk',
            'http://fsl.to/mjtt9t', 'http://fsl.to/0yywsu', 'http://fsl.to/seffef', 'http://fsl.to/8uism1',
            'http://fsl.to/ijlq9a', 'http://fsl.to/qnkat7', 'http://fsl.to/b8mfuv', 'http://fsl.to/j2u11b',
            'http://fsl.to/gry11r', 'http://fsl.to/6kfy25', 'http://fsl.to/iubn5f', 'http://fsl.to/bwzw7f',
            'http://fsl.to/s377of']


class Fsl:

    def __init__(self):
        self.base_url = 'http://fsl.to'
        self.ajax_url = '%s/%s' % (self.base_url, 'ajax_process.html')
        self.request = requests.session()
        self.retry = 1
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36'
        }
        self.captcha_file = 'fsl.png'
        self.captcha_data = None
        self.rk = RClient('caoyufei', 'hack3321', '109990', 'e42f4e0313f74f01b909fa0319c8ed7c')
        self.im_id = None

        self.down_path = os.path.dirname(os.path.realpath(__file__))
        res = self.fetch('http://78.46.102.94:8899/api/v1/proxies?page=1&limit=30')
        data = json.loads(res.text)
        self.proxy_list = data['proxies']

    def start(self):
        for proxy in self.proxy_list:
            proxy_info = 'http://%s:%s' % (proxy['ip'], proxy['port'])
            proxies = {'http': proxy_info, 'https': proxy_info}
            data = self._start(random.choice(fsl_list), proxies)
            if data:
                return data

    def _start(self, url, proxies):
        print('[+] start %s...' % url)
        print('proxy: %s' % proxies)
        res = self.fetch(url, proxies=proxies)
        if not res:
            return False

        html = res.text
        uid = re.search('uid="(\d+)"', html).group(1)
        captcha_code = self.get_captcha()
        if not captcha_code:
            return False

        params = {'ms': '696*210', 'sc': '1280*800', 'vrf': captcha_code, 'uid': uid}
        print(params)

        self.headers['X-Requested-With'] = 'XMLHttpRequest'
        self.headers['Referer'] = url
        self.retry = 3
        res = self.fetch(self.ajax_url, params, proxies=proxies)
        print(json.loads(res.text))
        return True

    def get_captcha(self):
        self._down_captcha()

        # im = open(self.captcha_file, 'rb').read()
        # res = self.rk.rk_create(im, '3040', self.captcha_file)
        # print(res)
        # self.im_id = res.get('Id')
        # if res.get('Result'):
        #     return res.get('Result')
        #
        # self.rk.rk_report_error(self.im_id)
        #
        # return False
        img = Image.open(self.captcha_file)
        img.show()
        return input('Please enter the captcha code:')

    def _down_captcha(self):
        r = self.fetch('%s/index_verify2.html' % self.base_url)
        with open(self.captcha_file, 'wb') as f:
            f.write(r.content)

    def fetch(self, url, data=None, **kwargs):
        response = None

        kwargs.setdefault('headers', self.headers)
        kwargs.setdefault('timeout', 20)
        # kwargs.setdefault('proxies', self.proxies)

        for i in range(self.retry):
            try:
                if data is None:
                    response = self.request.get(url, **kwargs)
                else:
                    response = self.request.post(url, data, **kwargs)
                if response.ok:
                    return response
            except Exception as e:
                time.sleep(1)
                print('retry [%s] %s %s' % (i, url, e))
                continue
        return response
