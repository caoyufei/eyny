from aip import AipOcr


class BaiDuOcr:

    def __init__(self):
        # self.APP_ID = '11614941'
        # self.API_KEY = 'olU11BcbanjqAkgxq8g17A2I'
        # self.SECRET_KEY = 'R9ZF6yAHsmkf2IBPifmBP0vTmEOFihhm'

        self.APP_ID = '11616231'
        self.API_KEY = 'MLqbYrpmGmhp0tgye08agmo3'
        self.SECRET_KEY = 'gZ2TTaHjfLVacWIVH7tRnkLakBXzwToK'
        self.client = AipOcr(self.APP_ID, self.API_KEY, self.SECRET_KEY)

    def _get_image(self, image_file):
        with open(image_file, 'rb') as f:
            image = f.read()
        return image

    def parse(self, image_file):
        image = self._get_image(image_file)

        data = self.client.basicGeneral(image)
        print('basicGeneral parse result: %s' % data)

        return self._return_result(data, image_file)

    def parse_accurate(self, image_file):
        image = self._get_image(image_file)

        data = self.client.basicAccurate(image)
        print('basicAccurate parse result: %s' % data)

        return self._return_result(data, image_file, False)

    def _return_result(self, data, image_file, is_accurate=True):
        if data.get('error_code'):
            return None

        words_result = data['words_result']
        if len(words_result) == 1:
            code = words_result[0]['words']
            if code.isdigit() and len(code) == 4:
                return code
            if is_accurate:
                return self.parse_accurate(image_file)
            return None

        word = list()
        for item in words_result:
            for i in item['words']:
                if not i.isdigit():
                    if is_accurate:
                        return self.parse_accurate(image_file)
                    return None
                word.append(i)
        return ''.join(word)
