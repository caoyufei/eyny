import os
import random

from common import random_str, md5, DIR_NAME, DIR_VIDEO_NAME, ROOT_PATH_VIDEO
from db import DB


class EynyFile:

    def __init__(self):
        pass

    def build_zip(self):
        db = DB()
        sql = "select * from ii_eyny where openload_url <> '' and files <> ''"
        items = db.execute(sql, (), False)
        i = 1
        n = len(items)
        random.shuffle(items)
        url_list = []
        for item in items:
            title = item['title']
            url_file = 'avhub8.url'
            pwd = random_str(5)
            video_file = item['files'].replace(DIR_NAME, DIR_VIDEO_NAME)

            zip_path = os.path.split(video_file)[0]
            zip_file = '%s/%s.zip' % (zip_path, md5(title))
            if os.path.isfile(zip_file):
                continue

            zip_cmd = "zip -P %s %s %s logo.png %s" % (pwd, zip_file, url_file, video_file)
            f = zip_file.replace(ROOT_PATH_VIDEO, '')
            zip_url = 'http://78.46.102.94:8090%s' % f
            print('[%s/%s]-------------------------------------------' % (i, n))
            print(zip_cmd)
            url_list.append(zip_url)
            os.system(zip_cmd)

            i = i + 1
            if i > 20:
                break

        for url in url_list:
            print(url)

    def _sub_file(self, lines, head, src_file, sub):
        [des_filename, ext_name] = os.path.splitext(src_file)
        filename = des_filename + '_' + str(sub) + ext_name
        print('make file: %s' % filename)
        out = open(filename, 'wb')
        try:
            out.writelines([head])
            out.writelines(lines)
            return sub + 1
        finally:
            out.close()

    def split_file(self, filename, count):
        fin = open(filename, 'rb')
        try:
            head = fin.readline()
            buf = []
            sub = 1
            for line in fin:
                buf.append(line)
                if len(buf) == count:
                    sub = self._sub_file(buf, head, filename, sub)
                    buf = []
            if len(buf) != 0:
                sub = self._sub_file(buf, head, filename, sub)
        finally:
            fin.close()
