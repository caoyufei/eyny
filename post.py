import json
import os
import random
import socket
import time

import socks
import telegram
from telegram import InputMediaPhoto

from common import BASE_API, fetch, PROXIES, MGSTAGE_PATH, md5, wget_down_file
from db import DB


class Post:

    def __init__(self):
        self.db = DB()

        if not PROXIES:
            self.char_id = '-1001389894760'
        else:
            self.char_id = '-1001284338996'

        self.TOKEN = '600262410:AAGusQsqTZt7Y4Oleq-h7E8Of8qEahUKUoY'
        self.base_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'mgstage')

    def post_te(self):
        sql = "SELECT * FROM ii_eyny where is_te = %s and openload_url <> %s"
        items = self.db.execute(sql, (0, ''), False)
        i = 1
        for item in items:
            if i > 20:
                break
            data = self.db.get_mgstage(item['title'])
            if not data:
                continue
            media = list()
            media.append(data['thumb'])
            media.extend(json.loads(data['images']))
            self.do_send_te(media, item['title'], [item['openload_url'], item['streamcherry_url']])
            i = i + 1
            time.sleep(random.randint(1024, 10240))

    def do_send_te(self, images, title, play_list: list):
        media_list = list()
        n = len(images)
        if n > 10:
            images = images[0:10]
            n = 10
        if PROXIES:
            socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", 1080)
            socket.socket = socks.socksocket
        print('[%s] to te' % title)
        html = '%s\n\n%s' % (title, '\n'.join(play_list))
        for image in images:
            if images[n - 1] == image:
                media = InputMediaPhoto(image, html, 'html')
            else:
                media = InputMediaPhoto(image)
            media_list.append(media)

        bot = telegram.Bot(self.TOKEN)

        for i in range(3):
            try:
                files = bot.sendMediaGroup(self.char_id, media_list, timeout=40)
                if len(files) >= n:
                    for file in files:
                        print('[%s] message id %s' % (title, file['message_id']))
                    print('----------------------------------------------------\n')
                    sql = 'update ii_eyny set is_te = 1 where title = %s'
                    self.db.execute(sql, (title,))
                break
            except Exception:
                time.sleep(5)
                continue

    def post_site(self):
        sql = "SELECT * FROM ii_eyny where is_post = %s and openload_url <> %s"
        items = self.db.execute(sql, (0, ''), False)
        i = 1
        n = len(items)
        for item in items:
            title = item['title']
            title_info = title.split('-')
            mgstage = self.db.get_mgstage(title)
            if not mgstage:
                print(title)
                continue
            thumbnail = mgstage['thumb']
            file_path = os.path.join(MGSTAGE_PATH, md5(title_info[0]))
            if not os.path.isdir(file_path):
                os.makedirs(file_path)
            thumbnail_file = os.path.join(file_path, '%s.jpg' % md5(thumbnail))
            if not os.path.isfile(thumbnail_file):
                wget_down_file(thumbnail, thumbnail_file)

            print('[%s/%s]-----------------------------------------------------' % (i, n))
            self._post_site(mgstage, title_info[0], thumbnail_file, item)
            i = i + 1

    def _post_site(self, data, type_id, thumbnail, eyny):
        title = '%s %s' % (data['title'], data['name'])
        params = {'title': title, 'slug': data['title'], 'c': type_id,
                  'content': '', 'thumbnail': thumbnail.replace(MGSTAGE_PATH, '/uploads')}
        url = BASE_API + '/api/post-save'
        res = fetch(url, params, proxies=None)
        print(res.text)

        self.do_post_meta(data['title'], [eyny['openload_url'], eyny['streamcherry_url']])

    def do_post_meta(self, title, meta_values):
        url = BASE_API + '/api/post-meta'
        params = {'type': 2, 'metas[]': meta_values, 'title': title}
        res = fetch(url, params, proxies=None)
        data = json.loads(res.text)
        if data['code'] != 0:
            print(data)
            return False
        return True
