import re
import time

import requests
from pyquery import pyquery


class ItoKoo:

    def __init__(self):
        self.base_url = 'http://www.itokoo.com'
        self.request = requests.session()
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'
        }

    def parse_post_list(self):
        page_url = '%s/thread.php?fid=108&page=1' % self.base_url
        res = self.fetch(page_url)
        res.encoding = 'gbk'
        doc = pyquery.PyQuery(res.text)
        elements = doc('#threadlist .tr3 .subject .subject_t')
        for element in elements.items():
            post_url = '%s/%s' % (self.base_url, element.attr('href'))
            self.parse_post(post_url)

    def parse_post(self, post_url):
        print('[+]: parse post: %s' % post_url)
        res = self.fetch(post_url)
        res.encoding = 'gbk'
        doc = pyquery.PyQuery(res.text)
        elements = doc('#readfloor_tpc .tpc_content a')
        pwd = re.search('百度网盘</a> 密码: ([^<]*)', res.text)
        if pwd:
            print(pwd.group(1).strip())
        for element in elements.items():
            print(element.attr('href'))

    def fetch(self, url, data=None, **kwargs):
        response = None
        kwargs.setdefault('headers', self.headers)
        kwargs.setdefault('timeout', 20)
        # kwargs.setdefault('proxies', PROXIES)

        for i in range(3):
            try:
                if data is None:
                    response = self.request.get(url, **kwargs)
                else:
                    response = self.request.post(url, data, **kwargs)
                if response.ok:
                    return response
            except Exception as e:
                time.sleep(1)
                print('retry [%s] %s %s' % (i, url, e))
                continue
        return response
