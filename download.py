import codecs
import os
import re

from common import ROOT_PATH, fm_vip_down, google_down_file, file_is_video
from db import DB
from upload import Upload


class DownLoad:

    def __init__(self):
        self.google_file = 0
        self.down_path = None
        self.title_id = ''
        self.db = DB()

    def run(self, title, limit):
        download_list = self.db.get_download(title, limit)
        if type(download_list) == dict:
            self.start(download_list['title'], eval(download_list['down_list']))
            return

        i = 1
        n = len(download_list)
        for item in download_list:
            print('---------------------------------------------------')
            print('[%s/%s] %s Downloading...' % (i, n, item['title']))
            self.start(item['title'], eval(item['down_list']))
            print('---------------------------------------------------')
            break

    def start(self, title, down_list: list):
        title_info = title.split('-')
        self.title_id = title_info[0]
        self.down_path = os.path.join(ROOT_PATH, self.title_id, title)

        if not os.path.isdir(self.down_path):
            os.makedirs(self.down_path)

        video_file = self.exists_file()
        if video_file:
            os.system("cd %s && rm -f `ls |grep -v '%s'`" % (self.down_path, video_file))
            print('file [%s] already exists!' % video_file)
            return

        n = len(down_list)
        self.google_file = 0

        for i, item in enumerate(down_list):
            i = i + 1
            down_url = item['down_url']
            pwd = item['pwd']
            print('\t(%s/%s) [%s] start downloading...' % (i, n, down_url))
            down_file = self._down_file(down_url, self.down_path)
            is_last = item == down_list[n - 1]

            if not down_file:
                print('\t(%s/%s) [%s] fail' % (i, n, down_url))
                if item != down_list[n - 1]:
                    continue

            if down_file:
                name = os.path.split(down_file)[1]
                print('\t[%s] download success! %s' % (name, is_last))

            # self._down_file_handler(down_file, pwd, title, is_last, n)

    def _down_file_handler(self, file, pwd, title, is_last, n):
        if file and file.find('part') == -1:
            file_size = os.path.getsize(file)
            file_size = file_size / float(1024 * 1024)
            if file_size < 1:
                self._parse_txt(file, pwd, title)

        if is_last:
            os.system("cd %s && rm -f *.url" % self.down_path)
            files = os.listdir(self.down_path)
            file_count = len(files)
            a = n - self.google_file
            if self.google_file > 0:
                a = a + 1

            if a == file_count or (file_count == 1 and files[0].find('part') == -1):
                self._un_rar_file(files[0], pwd, title)

    def _un_rar_file(self, file_name, pwd, title):
        cmd = self._get_un_rar_cmd(self.down_path, pwd, file_name)
        print('[unrar]: %s' % cmd)
        os.system(cmd)

        f = self.exists_file()
        if not f:
            print('[%s] video download fail!' % title)
            os.system('ls -lh %s' % self.down_path)
            return

        file = os.path.join(self.down_path, f)
        if f.find(title) == -1:
            info = f.split('.')
            new_file_name = '%s.%s' % (title, info[1])
            new_file = os.path.join(self.down_path, new_file_name)
            os.system("mv %s %s" % (file, new_file))
            file = new_file
            f = new_file_name

        os.system("cd %s && rm -f `ls |grep -v '%s'`" % (self.down_path, f))
        http_link = 'http://78.46.102.94:8090/%s/%s/%s' % (self.title_id, title, f)
        print('[http]: %s' % http_link)
        print('[file]: %s' % file)

        sql = 'update ii_eyny set files = %s where title = %s'
        self.db.execute(sql, (file, title))

        upload = Upload()
        upload.do_upload(http_link)

    def _parse_txt(self, file, pwd, title):
        cmd = self._get_un_rar_cmd(self.down_path, pwd, file)
        os.system(cmd)

        txt_file = os.path.join(self.down_path, title + '.txt')

        files = os.listdir(self.down_path)
        for f in files:
            _file = os.path.join(self.down_path, f)
            if _file.find('txt') != -1:
                txt_file = _file
                break

        if not os.path.isfile(txt_file):
            return

        with codecs.open(txt_file, 'r', 'utf-8') as f:
            txt_data = f.read()

        os.remove(txt_file)
        os.remove(file)

        # os.remove(txt_file)
        down_links = re.findall('[a-zA-z]+://[^\s]*', txt_data)
        for _down_link in down_links:
            if _down_link.find('google') != -1:
                print('download [%s] %s' % (title, _down_link))
                google_down_file(_down_link, self.down_path)

    def _down_file(self, down_url, down_path):
        down_method = None
        if down_url.find('feemoo') != -1:
            down_method = fm_vip_down(down_url, down_path)
        if down_url.find('google') != -1:
            self.google_file = self.google_file + 1
            down_method = google_down_file(down_url, down_path)
        return down_method

    def _get_un_rar_cmd(self, file_path, pwd, file):
        pwd_cmd = ''
        if pwd:
            pwd_cmd = "-p'%s'" % pwd
        return "cd %s && unrar e -y %s %s" % (file_path, pwd_cmd, file)

    def exists_file(self):
        files = os.listdir(self.down_path)
        for f in files:
            file = os.path.join(self.down_path, f)
            if file_is_video(file):
                return f
        return False

    def format_file(self, file_name):
        return os.path.join(self.down_path, file_name)
