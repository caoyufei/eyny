import json
import re
import time

from pyquery import pyquery
from requests.cookies import cookiejar_from_dict

from common import s, fetch
from db import DB


class Spider:

    def __init__(self):
        self.base_url = 'http://www23.eyny.com/'

        self.db = DB(2)
        self.login()

    def start(self):
        first_page_url = 'forum.php?mod=forumdisplay&fid=4264&filter=author&orderby=dateline'

        post_list = self.get_page_post_list(first_page_url)
        if len(post_list) <= 0:
            print('parse page post fail')
            return

        for item in post_list:
            if not self.db.get_eyny(item['title']):
                item = self.get_down_info(item)
                if not item:
                    continue

                down_list = json.dumps(item['down_list'])
                sql = 'insert into ii_eyny (title, post_url, down_list) values(%s, %s, %s)'
                last_id = self.db.execute(sql, (item['title'], item['post_url'], down_list))
                print('[%s] last id: %s\n%s' % (item['title'], last_id, down_list))

    def login(self, user_name='demo_001'):
        s.cookies = cookiejar_from_dict({})
        login_url = self.base_url + '/member.php?mod=logging&action=login&loginsubmit=yes&infloat=yes&lssubmit=yes&inajax=1'
        params = {"username": user_name, 'password': 'hack3321'}
        try:
            fetch(login_url, params)
        except Exception as e:
            print(e)

    def get_down_info(self, post_data):
        doc = self._parse_post(post_data)
        if not doc:
            return None

        html = doc('div.pcb:first')
        pwd = html('.blockcode div').text()
        links = html('a')
        if not links:
            print('parse post down error %s' % post_data['post_url'])
            return post_data

        time.sleep(2)
        post_data['down_list'] = list()
        pan_list = ['feemoo', 'fsl.to', 'google', '5xpan', 'pwpan', 'tadown', 'fmpan']

        for link in links.items():
            url = link.attr('href')
            for item in pan_list:
                if item in url:
                    post_data['down_list'].append({'down_url': url, 'pwd': pwd})
                    break

        if len(post_data['down_list']):
            return post_data
        return None

    def _parse_post(self, post_data):
        post_url = self.base_url + post_data['post_url']
        print('parse post: %s' % post_url)
        response = fetch(post_url)
        if response is None:
            print('parse post:[%s] error' % post_url)
            return None

        html = response.text
        if html.find('系統繁忙') != -1:
            print('系統繁忙中，請稍等幾秒再試')
            time.sleep(5)
            return None

        doc = pyquery.PyQuery(html)
        error = doc('.alert_error').text()
        if error:
            print(error)
            if '由於你瀏覽過多網頁' in error:
                exit(1)
            return None
        return doc

    def get_page_post_list(self, page_url):
        response = fetch(self.base_url + page_url)
        html = response.text
        if html.find('系统') != -1:
            time.sleep(10)
            self.get_page_post_list(page_url)
            return

        doc = pyquery.PyQuery(html)
        return self._get_page_post(doc)

    def _get_page_post(self, doc):
        post_list = list()
        elements = doc('#moderate table tbody')
        for element in elements.items():
            body_id = element.attr('id')
            if body_id.find('normalthread') == -1:
                continue

            post_url = element('a.xst').attr('href')
            re_res = re.findall('tid=(\d+)|page%3D(\w+)%26filter', post_url, re.M)
            if len(re_res) == 2:
                post_url = 'thread-%s-1-%s.html' % (re_res[0][0], re_res[1][1])

            title = element('a.xst').text()
            if title.find('求') != -1 or title.find('No') != -1:
                continue
            match = re.findall('(\w+-\d+)', title)
            if len(match) > 0 and post_url:
                _title = match[0]
                data = {'title': _title, 'post_url': post_url}
                post_list.append(data)
        return post_list
