# -*- coding: utf8 -*-
import argparse
import logging
import time

from common import ROOT_PATH, print_files, ROOT_PATH_VIDEO, reset_file
from convert import VideoConvert
from download import DownLoad
# ffmpeg -threads 4 -i /data/mgstage/259LUXU-279/259LUXU-279.mp4 -i /data/python/eyny/logo.png -filter_complex overlay=0:W-w -c:v libx264 -crf 20 -c:a aac -s 1280*720 /data/mgstage/259LUXU-279/259LUXU-279_logo.mp4
# ffmpeg -y -i /data/mgstage/SIRO-3361/SIRO-3361.mp4 -i /data/python/eyny/logo.png -filter_complex overlay=main_w-overlay_w-20:20 -c:v libx264 -crf 20 -c:a aac -s 1280*720 /data/mgstage/SIRO-3361/SIRO-3361_720.mp4
# ffmpeg -i /root/276KITAIKE-102.mp4 -s 854*480 -c:v libx264 -c:a aac -hls_time 120 out.m3u8
from femoo import FeMoo
from file import EynyFile
from gdrive import GDrive
from onedrive import OndDrive
from post import Post
from spider import Spider
from upload import Upload
from yunfile import YunFile


def main():
    print(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
    start_time = time.time()

    parser = argparse.ArgumentParser()
    # parser.add_argument('-t', '--title')
    parser.add_argument('--reset-upload', action='store_true')
    parser.add_argument('--reset-file', action='store_true')

    parser.add_argument('--remove-file', action='store_true')

    parser.add_argument('--google-drive', action='store_true')
    parser.add_argument('--one-drive', action='store_true')
    parser.add_argument('--yun-file', action='store_true')
    parser.add_argument('--feemoo-view', action='store_true')

    parser.add_argument('--build-zip', action='store_true')

    parser.add_argument('-d', '--download', action='store_true')
    download_grp = parser.add_argument_group('Download options')
    download_grp.add_argument('--down-title')
    download_grp.add_argument('--down-limit', type=int, default=10)

    parser.add_argument('-i', '--init', action='store_true')
    parser.add_argument('--pull', action='store_true')
    parser.add_argument('--post-site', action='store_true')
    parser.add_argument('--post-te', action='store_true')

    parser.add_argument('-c', '--convert', action='store_true')
    convert_grp = parser.add_argument_group('Convert options')
    convert_grp.add_argument('--convert-title')
    convert_grp.add_argument('--convert-limit', type=int, default=20)

    parser.add_argument('--list', action='store_true')
    parser.add_argument('-s', '--spider', action='store_true')

    args = parser.parse_args()
    # if args.title:
    #     _convert_video_by_title(args.title, path)

    if args.reset_upload:
        upload = Upload()
        upload.init_upload()

    if args.remove_file:
        delete_files()

    if args.build_zip:
        file = EynyFile()
        file.build_zip()

    if args.feemoo_view:
        fm = FeMoo()
        fm.view()

    if args.post_site:
        post = Post()
        post.post_site()

    if args.post_te:
        post = Post()
        post.post_te()

    if args.reset_file:
        reset_file(ROOT_PATH)
        reset_file(ROOT_PATH_VIDEO)

    if args.google_drive:
        g_drive_upload()

    if args.yun_file:
        yun = YunFile()
        yun.upload()

    if args.one_drive:
        one_drive_upload()

    if args.download:
        download(args.down_title, args.down_limit)

    if args.pull:
        upload = Upload()
        upload.update_open_load_cc()
        upload.update_stream_cherry()

    if args.convert:
        VideoConvert().start(args.convert_title, args.convert_limit)

    if args.list:
        print_files()

    if args.spider:
        post_spider()

    t = time.time() - start_time
    print("costTime: %.2fs" % t)


def g_drive_upload():
    drive = GDrive()
    drive.start_upload()


def one_drive_upload():
    drive = OndDrive()
    drive.start_update()


def post_spider():
    spider = Spider()
    spider.start()


def delete_files():
    pass


def download(title, limit):
    down = DownLoad()
    down.run(title, limit)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(message)s')
    # load_page_list()
    main()
    # hk0fc7sj7@od.office365vip.cn
    # syhxamrkm@od.office365vip.cn
    # b16sy7v77@1.jjkl.ac.nz
