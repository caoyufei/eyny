import base64
import json
import os
import random
import re
import sys
import time
from concurrent import futures
from contextlib import closing

import requests
from PIL import Image
from pyquery import pyquery

from common import PROXIES

down_list = ['http://www.fmpan.com/file-2366623.html', 'http://www.fmpan.com/file-2366632.html',
             'http://www.fmpan.com/file-2367048.html', 'http://www.fmpan.com/file-2369626.html',
             'http://www.fmpan.com/file-2369628.html', 'http://www.fmpan.com/file-2369629.html',
             'http://www.fmpan.com/file-2369633.html', 'http://www.fmpan.com/file-2369642.html',
             'http://www.fmpan.com/file-2369647.html', 'http://www.fmpan.com/file-2369648.html',
             'http://www.fmpan.com/file-2369653.html', 'http://www.fmpan.com/file-2370279.html',
             'http://www.fmpan.com/file-2370288.html', 'http://www.fmpan.com/file-2370289.html',
             'http://www.fmpan.com/file-2370296.html', 'http://www.fmpan.com/file-2370309.html',
             'http://www.fmpan.com/file-2370316.html', 'http://www.fmpan.com/file-2370321.html',
             'http://www.fmpan.com/file-2370328.html', 'http://www.fmpan.com/file-2370331.html',
             'http://www.fmpan.com/file-2370334.html', 'http://www.fmpan.com/file-2371810.html',
             'http://www.fmpan.com/file-2371823.html', 'http://www.fmpan.com/file-2372027.html',
             'http://www.fmpan.com/file-2372099.html']
agent_list = [
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1.1 Safari/605.1.15',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:61.0) Gecko/20100101 Firefox/61.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0',
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',
    'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E; Zoom 3.6.0)',
    'Mozilla/5.0 (compatible; MSIE 10.6; Windows NT 6.1; Trident/5.0; InfoPath.2; SLCC1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 2.0.50727) 3gpp-gba UNTRUSTED/1.0'
]


class FeMoo:

    def __init__(self):
        self.base_url = 'http://www.feemoo.com'
        self.ajax_url = '%s/%s' % (self.base_url, 'ajax.php')
        self.request = requests.session()
        self.headers = {
            'User-Agent': random.choice(agent_list)
        }
        self.captcha_file = 'femoo.png'
        self.captcha_data = None
        self.down_path = os.path.dirname(os.path.realpath(__file__))
        self.proxies = {
            'http': 'http://78.46.102.94:8081',
            'https': 'http://78.46.102.94:8081',
        }
        res = self.fetch('http://205.139.16.25:8899/api/v1/proxies?page=1&limit=30')
        data = json.loads(res.text)
        self.proxy_list = data['proxies']

    def upload(self, file):
        res = self.fetch(
            'http://www.feemoo.com/plugin_upload.php?uid=1904379&folder_id=0&plugin_type=dx2&hash=441d87864a21cbd598a73c86bb913277')
        params = re.search('post_params:([^}]*)', res.text).group(1) + '}'
        params = eval(params)
        print(params)

        upload_url = 'http://upd28.ihuolong.net/rc_upload.php'
        file_info = os.path.split(file)
        # params['upload_file'] = (file_info[1], open(file, 'rb'))
        # params['Filename'] = 'FCDE84BB0E68E9DC.zip'
        # m = MultipartEncoder(params)
        files = {'upload_file': open(file, 'rb')}
        res = self.fetch(upload_url, params, files=files)
        print(res)

    def view(self):
        with futures.ThreadPoolExecutor(5) as executor:
            executor.map(self._start_view, down_list)

    def _start_view(self, url):
        print('[+]: start view... %s' % url)
        res = self.fetch(url, proxies=self.proxies)
        if not res:
            return False
        html = res.text
        if html.find('doudbtn2') == -1:
            return False

        doc = pyquery.PyQuery(html)
        down_link = doc('.doudbtn2').attr('href')
        if not down_link:
            return False

        self.headers['Referer'] = url
        new_down_link = '%s/%s' % (self.base_url, down_link)
        print(new_down_link)
        res = self.fetch(new_down_link, proxies=self.proxies)
        print(res.headers)
        return True

    def download(self, url, proxies=None):
        print('[+]: start downloading... %s' % url)
        file_id = re.search('(\d+)', url).group(1)
        if proxies:
            global PROXIES
            PROXIES = proxies

        res = self.fetch(url)
        doc = pyquery.PyQuery(res.text)
        down_link = doc('.doudbtn2').attr('href')
        if not down_link:
            return

        self.headers['Referer'] = url

        new_down_link = '%s/%s' % (self.base_url, down_link)
        print(new_down_link)
        res = self.fetch(new_down_link)
        html = res.text

        self._wait_time()
        captcha_code = self.get_captcha()

        params = {'action': 'load_down_addr_com', 'file_id': file_id, 'verycode': captcha_code,
                  'codeencry': self.captcha_data['code']}

        self.headers['X-Requested-With'] = 'XMLHttpRequest'
        self.headers['Referer'] = new_down_link

        print(params)
        res = self.fetch(self.ajax_url, params)
        if not res:
            print(res)
            return

        data = json.loads(res.text)
        down_link = data['str']
        if down_link.find('http') == -1:
            print(data)
            return

        pc_ = re.search('action=pc_(\d+)&', html).group(1)
        params = {'action': 'pc_%s' % pc_, 'file_id': file_id, 'ms': '724*427', 'sc': '1280*800'}
        print(params)
        self.fetch(self.ajax_url, params)
        self._down_file(down_link)

    def _down_file(self, url):
        with closing(self.fetch(url, stream=True)) as r:
            print(r.headers)

    def get_captcha(self):
        self._down_captcha()

        img = Image.open(self.captcha_file)
        img.show()
        return input('Please enter the captcha code:')

    def _down_captcha(self):
        r = self.fetch('%s/new_imgcode.php' % self.base_url, {'act': 'downvf'})
        data = json.loads(r.text)
        self.captcha_data = data

        base = data['base']
        base_str = base.replace('data:image/png;base64', '')
        img_data = base64.b64decode(base_str)
        with open(self.captcha_file, 'wb') as f:
            f.write(img_data)

    def _wait_time(self):
        for i in range(1, 17):
            sys.stdout.write('\rwait time: ' + str((16 - i)))
            sys.stdout.flush()
            time.sleep(1)
        print('')

    def fetch(self, url, data=None, **kwargs):
        response = None
        kwargs.setdefault('headers', self.headers)
        kwargs.setdefault('timeout', 20)
        # kwargs.setdefault('proxies', PROXIES)

        for i in range(3):
            try:
                if data is None:
                    response = self.request.get(url, **kwargs)
                else:
                    response = self.request.post(url, data, **kwargs)
                if response.ok:
                    return response
            except Exception as e:
                time.sleep(1)
                print('retry [%s] %s %s' % (i, url, e))
                continue
        return response
