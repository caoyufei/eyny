import json
import os

from common import fetch, ROOT_PATH
from db import DB


class Upload:

    def __init__(self):
        self.open_load_cc_key = 'h0Q-lVSU'
        self.open_load_cc_login = '50360562d32ba9cb'

        self.stream_cherry_id = 'CYJtMhQcnI'
        self.stream_cherry_pwd = 'gRoqohvn'

        self.rapid_video_uid = '100773'
        self.db = DB()

    def upload_one_drive(self, file):
        cmd = '/usr/bin/php /data/wwwroot/default/oneindex/one.php upload:file %s /mgstage/' % file
        os.system(cmd)

    def upload_google_drive(self, file):
        cmd = 'rclone copy %s pod78:%s' % (file, '/mgstage/')
        os.system(cmd)

    def rapid_video_upload(self, url):
        api = 'https://api.rapidvideo.com/v1/remote.php?ac=add&user_id=%s&url=%s' % (self.rapid_video_uid, url)
        res = fetch(api)
        return self._get_result(res.text)

    def stream_cherry_upload(self, url):
        api = 'https://api.fruithosted.net/remotedl/add?login=%s&key=%s&url=%s&folder=%s' % (
            self.stream_cherry_id, self.stream_cherry_pwd, url, '')
        res = fetch(api)
        return self._get_result(res.text)

    def open_load_cc_upload(self, url):
        api = 'https://api.openload.co/1/remotedl/add?login=%s&key=%s&url=%s' % (
            self.open_load_cc_login, self.open_load_cc_key, url)
        res = fetch(api)

        return self._get_result(res.text)

    def get_stream_cherry_url(self):
        api = 'https://api.fruithosted.net/file/listfolder?login=%s&key=%s' % (
            self.stream_cherry_id, self.stream_cherry_pwd)
        res = fetch(api)
        data = json.loads(res.text)
        if data['status'] == 200:
            return data['result']['files']
        return None

    def get_open_load_cc_url(self):
        check_api = 'https://api.openload.co/1/file/listfolder?login=%s&key=%s' % (
            self.open_load_cc_login, self.open_load_cc_key)
        res = fetch(check_api)

        data = json.loads(res.text)
        if data['status'] == 200:
            return data['result']['files']
        return None

    def delete_open_load_cc(self, file_id):
        check_api = 'https://api.openload.co/1/file/delete?login=%s&key=%s&file=%s' % (
            self.open_load_cc_login, self.open_load_cc_key, file_id)
        res = fetch(check_api)

        data = json.loads(res.text)
        if data['status'] == 200:
            return data
        return None

    def update_open_load_cc(self):
        items = self.get_open_load_cc_url()
        data = dict()
        for item in items:
            name = item.get('name').split('.')[0]
            link = item.get('link').replace('/f', '/embed')
            if not data.get(name):
                data[name] = link
                row = self._update_db('openload_url', link, name)
                if not row:
                    print('update openloadcc url [%s] fail' % name)
            else:
                self.delete_open_load_cc(item['linkextid'])

    def update_stream_cherry(self):
        items = self.get_stream_cherry_url()
        data = dict()
        for item in items:
            name = item.get('name').split('.')[0]
            link = item.get('link').replace('/f', '/embed')
            if not data.get(name):
                data[name] = link
                row = self._update_db('streamcherry_url', link, name)
                if not row:
                    print('update streamcherry url [%s] fail' % name)
            else:
                print('[%s] [%s] multiple' % (name, item['linkextid']))

    def _update_db(self, field, value, title):
        sql = 'update ii_eyny set %s = %s where title = %s'
        return self.db.execute(sql, ())

    def init_upload(self):
        sql = "select * from ii_eyny where files <> '' and (streamcherry_url = '' or openload_url = '')"
        items = self.db.execute(sql, (), False)
        for item in items:
            file = str(item['files']).replace(ROOT_PATH, '')
            http_link = 'http://78.46.102.94:8090%s' % file
            print(http_link)

            if str(item['openload_url']).find('http') == -1:
                open_load_url = self.open_load_cc_upload(http_link)
                if open_load_url and open_load_url != 0:
                    print('uplaod to openload success! return: %s' % open_load_url)

            if str(item['streamcherry_url']).find('http') == -1:
                stream_cherry_url = self.stream_cherry_upload(http_link)
                if stream_cherry_url and stream_cherry_url != 0:
                    print('uplaod to streamcherry success! return: %s' % stream_cherry_url)

            print('---------------------------------------')

    def _get_result(self, result):
        data = json.loads(result)
        if data['status'] == 200:
            return data['result']['id']

        print(result)
        return None

    def do_upload(self, http_link):
        open_load_url = self.open_load_cc_upload(http_link)
        if open_load_url and open_load_url != 0:
            print('uplaod to openload success! return: %s' % open_load_url)

        stream_cherry_url = self.stream_cherry_upload(http_link)
        if stream_cherry_url and stream_cherry_url != 0:
            print('uplaod to streamcherry success! return: %s' % stream_cherry_url)

        # rapid_video_url = self.rapid_video_upload(http_link)
        # if rapid_video_url and rapid_video_url != 0:
        #     print('uplaod to rapidvideo success! return: %s' % rapid_video_url)
