import os
from concurrent import futures

from common import ROOT_PATH, DIR_VIDEO_NAME, DIR_NAME, file_is_video, get_file_size, ROOT_PATH_VIDEO


class VideoConvert:

    def __init__(self):
        pass

    def start(self, title=None, limit=20):
        task_list = self.get_tasks(title)
        if len(task_list) <= 0:
            print('no task')
            return

        if len(task_list) > limit:
            task_list = task_list[0:limit]

        with futures.ThreadPoolExecutor(2) as executor:
            for task in task_list:
                executor.submit(self.convert_video, task[0], task[1], task[2])

    def convert_video(self, in_file, out_file, name):
        # root_info = os.path.split(root)
        # title = root_info[1]
        # title_info = title.split('-')
        #
        # path_info = file.split('.')

        if not os.path.isfile(out_file):
            print('[+]: ffmpeg [%s:%s] begin-----------' % (name, get_file_size(in_file)))
            cmd = 'ffmpeg -y -threads 4 -i %s -i /data/python/eyny/logo.png -filter_complex overlay=main_w-overlay_w-20:20 -c:v libx264 -crf 20 -c:a copy -s 1280*720 %s' % (
                in_file, out_file)
            # print(cmd)
            os.system(cmd)

        if os.path.isfile(out_file):
            print('[+]: ffmpeg [%s:%s] end-----------' % (os.path.split(out_file)[1], get_file_size(out_file)))

        #     os.remove(file)
        #     db = DB()
        #     db.update_files(title, out_file)
        #     db.close()
        #
        #     file_name = os.path.split(out_file)[1]
        #     http_link = 'http://78.46.102.94:8090/%s/%s/%s' % (title_info[0], title, file_name)
        #     print('  [http]: %s' % http_link)

        # file_mp4 = os.path.join(os.path.dirname(os.path.realpath(__file__)), '1.mp4')
        # p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        # (stdout, stderr) = p.communicate()
        # stdout = str(stdout)
        # find = re.search('((\d{2,4})x(\d{2,4}))', stdout)
        # print(find[0])

    def get_tasks(self, title=None):
        if title:
            return self.get_video_by_title(title)

        return self._get_all_video()

    def get_video_by_title(self, title):
        task_list = list()

        title_info = title.split('-')
        title_id = title_info[0]

        path = os.path.join(ROOT_PATH, title_id, title)
        files = os.listdir(path)
        for f in files:
            file = os.path.join(path, f)
            self._put_task(file, f, task_list)

        return task_list

    def _get_all_video(self):
        task_list = list()
        for (root, dirs, files) in os.walk(ROOT_PATH):
            for name in files:
                file = os.path.join(root, name)
                self._put_task(file, name, task_list)
        return task_list

    def _put_task(self, file, name, task_list: list):
        convert_file = file.replace(DIR_NAME, DIR_VIDEO_NAME)
        if file_is_video(file) and not os.path.isfile(convert_file):
            out_path = os.path.split(convert_file)[0]
            if not os.path.isdir(out_path):
                os.makedirs(out_path)

            task_list.append((file, convert_file, name))
