import cgi
import codecs
import os
import random
import re
import sys
import time
from urllib.parse import urlparse, unquote

import requests
from PIL import Image
from pyquery import pyquery

from common import random_str, md5, PROXIES, DIR_NAME, DIR_VIDEO_NAME
from db import DB
from ocr import BaiDuOcr


class YunFile:

    def __init__(self):
        self.base_url = 'http://page2.dfpan.com'
        self.request = requests.session()
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36'
        }
        self.ocr = BaiDuOcr()
        self.captcha_file = 'yunfile.jpg'
        self.down_path = os.path.dirname(os.path.realpath(__file__))
        self.user = 'caoyufei'
        self.pwd = 'hack3321'
        self.zip_path = '/data/drive/zip'

        if PROXIES:
            self.zip_path = os.path.join(self.down_path, 'logs')

        if not os.path.isdir(self.zip_path):
            os.makedirs(self.zip_path)

    def login(self):
        url = 'http://www.yunfile.com/member/fallineLogin.html'
        res = self.fetch(url)
        doc = pyquery.PyQuery(res.text)
        form = doc('#falline_login form')
        params = {}
        for _input in form('input'):
            element = pyquery.PyQuery(_input)
            params[element.attr('name')] = element.attr('value') or ''

        params['username'] = self.user
        params['password'] = self.pwd
        params['que'] = 0
        params['remember'] = 'on'
        print(params)
        print(form.attr('action'))
        self.headers['Referer'] = url

        res = self.fetch(form.attr('action'), params)

    def _start_upload(self, file):
        # self.login()
        url = 'http://www.yunfile.com/upload/dzupload.html'
        res = self.fetch(url)
        html = res.text
        if html.find('fileUploadForm') == -1:
            return
        doc = pyquery.PyQuery(html)
        form = doc('#fileUploadForm')
        action = form.attr('action')
        action = action.replace('common', self.user)
        action = action.replace('guest', self.user)

        file_info = os.path.split(file)
        files = [
            ('file', (file_info[1], open(file, 'rb'))),
        ]
        self.headers['Referer'] = url
        res = self.fetch(action, {'userId': self.user, 'parentPath': '/share', 'autoDir': False}, files=files)
        html = res.text
        down_link = re.search('filedownurl = unescape\("([^"]*)', html)
        if down_link:
            return unquote(down_link.group(1))
        print(html)
        return None

    def upload(self):
        db = DB()
        sql = "select * from ii_eyny where openload_url <> '' and zip_pwd = ''"
        items = db.execute(sql, (), False)
        i = 1
        n = len(items)
        random.shuffle(items)
        for item in items:
            title = item['title']
            txt_content = '下载地址：\n%s\n%s\ntelegram群:https://t.me/avhub8\n更多请访问: https://www.avhub8.com' % (
                item['openload_url'], item['streamcherry_url'])
            txt_file = '%s/%s.txt' % (self.zip_path, title)
            url_file = 'avhub8.url'
            with codecs.open(txt_file, 'w', encoding='utf-8') as f:
                f.write(txt_content)

            print('-------------------------------------------------------------')
            print('(%s/%s) [%s] start uploading...' % (i, n, title))
            pwd = random_str(3)
            zip_file = '%s/%s.zip' % (self.zip_path, md5(title))
            zip_cmd = "zip -P %s %s %s %s logo.png" % (pwd, zip_file, txt_file, url_file)
            if not os.path.isfile(zip_file):
                print(zip_cmd)
                os.system(zip_cmd)
            os.remove(txt_file)
            # if os.path.isfile(zip_file) and os.path.getsize(zip_file) > 10:
            #     link = self._start_upload(zip_file)
            #     if link and link.find('fs') != -1:
            #         sql = 'update ii_eyny set zip_url = %s, zip_pwd = %s where id = %s'
            #         row = db.execute(sql, (link, pwd, item['id']))
            #         print('upload yunfile: %s %s' % (row, link))
            i = i + 1
            if i > 20:
                break

    def download(self, url, down_path=None):
        print('[+]: start downloading... %s' % url)
        res = self.fetch(url, allow_redirects=False)
        if res.status_code != 200:
            url = res.headers['location']
            res = self.fetch(res.headers['location'])

        url_info = urlparse(url)
        self.base_url = '{}://{}'.format(url_info.scheme, url_info.netloc)
        self.headers['Referer'] = url

        if down_path:
            self.down_path = down_path

        html = res.text
        counter = re.search('http://www.yunfile.com/counter.jsp[^"]*', html)
        if counter:
            self.fetch(counter.group())

        for i in range(1):
            try:
                if self._start_down(html):
                    # os.remove(self.captcha_file)
                    break
            except Exception as e:
                print(e)

    def _start_down(self, html):
        if html.find('down_interval_tag') != -1:
            doc = pyquery.PyQuery(html)
            slow_time = doc('#down_interval_tag').text()
            if slow_time:
                print('download limit, wait %s minute!' % slow_time)
                time.sleep(60 * int(slow_time))

        match = re.search('var url = "(.+?)"', html)
        file_url = match.group(1)

        captcha_code = self.get_captcha()
        file_url = file_url.replace('.html', '/%s.html' % captcha_code)
        down_url = '%s%s' % (self.base_url, file_url)

        self._wait_time()

        res = self.fetch(down_url, allow_redirects=False)
        if res.status_code == 200:
            return self.build_form(res.text)

        print(down_url, res.headers['location'])
        return False

    def build_form(self, html):
        params = {}
        doc = pyquery.PyQuery(html)
        form = doc('#d_down_from')
        for _input in form("input"):
            element = pyquery.PyQuery(_input)
            params[element.attr('name')] = element.attr('value')

        try:
            cdn_url = re.search('saveCdnUrl="([^"]*)', html).group(1)
            action = re.search('saveCdnUrl\+"(\S+)";', html).group(1)
            params['vid'] = re.search('vericode = "(\w+)";', html).group(1)
            params['fileId'] = re.search('form.fileId.value = "(\w+)";', html).group(1)
            js = 'http://www.yunfile.com/ckcounter.jsp?userId=caoyufei&LargeTag=%s' % params['fileId']
            self.fetch(js)

            down_url = cdn_url + action
            res = self.fetch(down_url, params, allow_redirects=False)
            headers = res.headers
            if res.status_code != 200:
                return False

            a, v = cgi.parse_header(headers['Content-Disposition'])
            file_name = os.path.join(self.down_path, v["filename"])
            with open(file_name, 'wb') as f:
                f.write(res.content)

            print('download done, file: %s' % file_name)
        except Exception as e:
            print(e)
            return False
        return True

    def get_captcha(self):
        self._down_captcha()
        code = self.ocr.parse(self.captcha_file)
        if code:
            print('ocr parse result: %s' % code)
            return code

        img = Image.open(self.captcha_file)
        img.show()
        return input('Please enter the captcha code:')

    def _down_captcha(self):
        r = self.fetch('%s/verifyimg/getPcv.html' % self.base_url, proxies=None, headers=self.headers)
        with open(self.captcha_file, 'wb') as f:
            f.write(r.content)

    def _wait_time(self):
        for i in range(1, 31):
            sys.stdout.write('\rwait time: ' + str((30 - i)))
            sys.stdout.flush()
            time.sleep(1)
        print('')

    def fetch(self, url, data=None, **kwargs):
        response = None

        kwargs.setdefault('headers', self.headers)
        kwargs.setdefault('timeout', 20)
        # kwargs.setdefault('proxies', PROXIES)

        for i in range(3):
            try:
                if data is None:
                    response = self.request.get(url, **kwargs)
                else:
                    response = self.request.post(url, data, **kwargs)
                if response.ok:
                    return response
            except Exception as e:
                time.sleep(1)
                print('retry [%s] %s %s' % (i, url, e))
                continue
        return response
