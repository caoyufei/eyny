import os
import random
import time

from common import fetch, PROXIES
from ctfile import CtFile
from femoo import FeMoo, down_list
from file import EynyFile
from fsl import Fsl

# post = Post()
# post.post_site()
from itokoo import ItoKoo

femoo = FeMoo()
_file = EynyFile()

# down_lists = down_list
#
#
# for proxy in femoo.proxy_list:
#     proxy_info = 'http://%s:%s' % (proxy['ip'], proxy['port'])
#     proxies = {'http': proxy_info, 'https': proxy_info}
#     femoo.download(random.choice(down_lists), proxies)

# femoo.download('http://www.feemoo.com/file-2374372.html')
file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'logs', '')
# femoo.upload(file)


fsl = Fsl()
# fsl.start()

itokoo = ItoKoo()
# itokoo.parse_post_list()
# itokoo.parse_post('http://www.itokoo.com/read.php?tid=24353')
# itokoo.parse_post('http://www.itokoo.com/read.php?tid=36463')

ct = CtFile()
# ct.download('https://itokoo.ctfile.com/fs/10048483-299469871')

if __name__ == '__main__':
    begin = time.time()
    file = os.path.join(os.path.expanduser('~'), 'downloads', 'E2DF08CEE3ED2D95.zip')
    # _file.split_file(file, 220000)
    end = time.time()
