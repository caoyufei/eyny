# -*- coding: utf8 -*-
import argparse
import json
import os
import re
import time
import traceback
from concurrent import futures
from concurrent.futures import Future

import pyquery

from common import fetch, s, PROXIES, LOG_PATH, md5, write_file, MGSTAGE_PATH, wget_down_file
from db import DB

headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36',
    'Cookie': 'adc=1;',
}

page_size = 120
image_count = 0

TYPE_LIST = {'nanpatv': 14, 'documentv': 1, 'ara': 3, 'prestigepremium': 5, 'luxutv': 9, 'dokikaku': 1,
             'orenoshirouto': 7, 'scute': 5, 'shirouto': 28}

base_url = 'http://www.mgstage.com'
page_url = '/search/search.php?search_word=&image_word_ids[]=%s&sort=new&list_cnt=%s&disp_type=thumb&page=%s'

POST_LIST = list()
BASE_PATH = MGSTAGE_PATH

s.cookies.set('adc', '1')

db = DB()


def get_type_page_list():
    type_list = list()
    _count = db.get_mgstage_count()
    for (word_id, page_count) in TYPE_LIST.items():
        for page in range(1, page_count + 1):
            full_page_url = base_url + page_url % (word_id, page_size, page)
            type_list.append(full_page_url)
            if PROXIES or _count > 7500:
                break
        if PROXIES:
            break
    return type_list


def parse_type_page(url):
    print('[+] parse page: %s' % url)

    res = fetch(url)
    if res is None:
        return []

    post_list = list()
    doc = pyquery.PyQuery(res.text)
    elements = doc('div.rank_list li h5 a')
    for element in elements.items():
        post_url = element.attr('href')
        post_list.append(post_url)

    return post_list


def parse_post_list(future: Future):
    post_list = future.result()
    with futures.ThreadPoolExecutor(10) as executor:
        executor.map(parse_post, post_list)


def parse_post(post_url):
    print('[+] parse post: %s' % post_url)

    res = fetch(base_url + post_url)
    if res is None:
        print('[+] parse post: %s fail' % post_url)
        parse_post(post_url)
        return

    doc = pyquery.PyQuery(res.text)
    tag = doc('h1.tag')
    table = doc('table[style="clear:both;"]')
    thumbs = doc('a.sample_image')
    (pid, type_id) = get_post_id(post_url)
    title = tag.text() + table('td:eq(0)').text()
    thumbnail = doc('#EnlargeImage').attr('href')
    images = []
    for thumb in thumbs.items():
        images.append(thumb.attr('href'))

    if len(images) < 3:
        images = []
        thumbnail_info = re.search('images/(\w+)/', thumbnail)
        image_temp = 'http://image.mgstage.com/images/%s/%s/%s/cap_e_%s_%s.jpg'
        for i in range(5):
            title_info = str(pid).split('-')
            image = image_temp % (thumbnail_info.group(1), title_info[0], title_info[1], i, str(pid).lower())
            images.append(image)

    if len(images) > 9:
        images = images[0:9]

    save_info(type_id, pid, title, thumbnail, images)


def save_info(type_id, pid, title, thumbnail, images):
    file_path = os.path.join(BASE_PATH, md5(type_id))
    if not os.path.exists(file_path):
        os.makedirs(file_path)

    thumbnail_file = os.path.join(file_path, '%s.jpg' % md5(thumbnail))
    if not os.path.exists(thumbnail_file):
        print('downloading %s ...' % thumbnail)
        wget_down_file(thumbnail, thumbnail_file)

    new_images = images
    # for image in images:
    #     image_file = os.path.join(file_path, '%s.jpg' % md5(image))
    #     f = down_file(image, image_file)
    #     if f:
    #         new_images.append(image)

    POST_LIST.append({'pid': pid, 'thumbnail': thumbnail, 'images': images, 'title': title})

    try:
        save_db(thumbnail, images, pid, title)
    except Exception as e:
        print('save db error: %s' % e)
        traceback.print_exc()


def get_post_id(post_url):
    _list = post_url.split('/')
    pid = _list[-2]
    type_id = pid.split('-')[0]
    return pid, type_id


def save_db(thumbnail, images, title, name):
    row = db.get_mgstage(title)
    if len(images):
        images = json.dumps(images)
    else:
        images = ''
    if len(name) > 150:
        name = str(name[0:150])

    if not row:
        sql = 'insert into ii_mgstage(title, thumb, images, name) values (%s, %s, %s,%s)'
        last_id = db.execute(sql, (title, thumbnail, images, name))
        print('[%s] insert lastId: %s' % (title, last_id))
    else:
        sql = 'update ii_mgstage set thumb=%s, images=%s where title = %s'
        row = db.execute(sql, (thumbnail, images, title))
        print('[%s] update rows: %s' % (title, row))


def main():
    start_time = time.time()

    type_page_list = get_type_page_list()
    with futures.ThreadPoolExecutor(5) as executor:
        for type_page in type_page_list:
            future = executor.submit(parse_type_page, type_page)
            future.add_done_callback(parse_post_list)

    t = time.time() - start_time
    print("costTime: %.2fs" % t)


if __name__ == '__main__':
    main()

    # save_video()
    # time.strftime("%Y%m%d", time.localtime())
